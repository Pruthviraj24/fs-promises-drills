const fs = require("fs");

function problem2() {
  fs.promises.readFile("./test/lipsum.txt", "utf-8").then((resolveLipsum) => {
    const upperCaseData = resolveLipsum.toUpperCase();
    const newFile1 = "firstFile.txt";
    fs.promises.writeFile(`${newFile1}`, upperCaseData).then(() => {
      fs.promises.writeFile("filenames.txt", newFile1).then(() => {
        fs.promises.readFile(newFile1, "utf-8").then((resolveFile1) => {
          const lowerCaseData = resolveFile1.toLowerCase().split(".");
          const newFile2 = "secondFile.txt";
          fs.promises.writeFile(`${newFile2}`, lowerCaseData).then(() => {
            fs.promises.appendFile("filenames.txt", `-${newFile2}`).then(() => {
              fs.promises
                .readFile(`${newFile1}`, "utf-8")
                .then((resolveFileone) => {
                  fs.promises
                    .readFile(`${newFile2}`, "utf-8")
                    .then((resolveFiletwo) => {
                      const sortedData = (resolveFileone + resolveFiletwo)
                        .split("")
                        .sort()
                        .join("");
                      const sortedText = "sortedText.txt";
                      fs.promises
                        .writeFile(`${sortedText}`, sortedData)
                        .then(() => {
                          fs.promises
                            .appendFile("filenames.txt", `-${sortedText}`)
                            .then(() => {
                              fs.promises
                                .readFile("filenames.txt", "utf-8")
                                .then((resovleFileName) => {
                                  const arrayOfFileName =
                                    resovleFileName.split("-");
                                  for (let file of arrayOfFileName) {
                                    fs.promises
                                      .unlink(`${file}`)
                                      .then((res) => {
                                        console.log(`Deleted ${file}`);
                                      })
                                      .catch((err) => {
                                        console.log(err);
                                      });
                                  }
                                })
                                .catch((err) => {
                                  console.log(err);
                                });
                            });
                        });
                    });
                });
            });
          });
        });
      });
    });
  });
}

module.exports = problem2;
