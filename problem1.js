const fs = require("fs");

function problem1() {
  const createRandomPromise = fs.promises.mkdir("reandomFiles");
  const writeIntoJsonFile = fs.promises.writeFile(
    "./reandomFiles/random.json",
    JSON.stringify("{name:raj}")
  );
  const deleteFile = fs.promises.unlink("./reandomFiles/random.json");

  createRandomPromise.then(() => {
    writeIntoJsonFile.then(() => {
      deleteFile
        .then(() => {
          console.log("Created and Deleted file");
        })
        .catch((err) => {
          console.log(err);
        });
    });
  });
}

module.exports = problem1;
